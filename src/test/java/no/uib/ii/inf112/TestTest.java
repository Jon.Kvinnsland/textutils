package no.uib.ii.inf112;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class TestTest {
	TextAligner aligner = new TextAligner() {

		public String center(String text, int width) {
			int extra = (width - text.length()) / 2;
			return " ".repeat(extra) + text + " ".repeat(extra);
		}


		public String flushRight(String text, int width) {
			int extra = width - text.length();
			return " ".repeat(extra) + text;
		}

		public String flushLeft(String text, int width) {
			int extra = width - text.length();
			return text + " ".repeat(extra);
		}

		public String justify(String text, int width) {
			// TODO Auto-generated method stub
			
			String stringStripped = text.strip();
			

			return null;
		}};
		
	@Test
	void testFlushRight() {
		assertEquals("     A", aligner.flushRight("A", 5));
	}

	void testFlushLeft() {
		assertEquals("A     ", aligner.flushRight("A", 5));
	}

	@Test
	void testCenter() {
		assertEquals("  A  ", aligner.center("A", 5));
		assertEquals(" foo ", aligner.center("foo", 5));
	}
}
